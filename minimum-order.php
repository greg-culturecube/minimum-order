<?php
/**
 *Plugin Name: Minimum Order
 *Description: Set a minimum order amount for checkout
 */

class MinimumOrder {

    private $minimum_value = 0;

    public function __construct() {
        add_action('admin_menu', array($this, 'create_settings_page'));
        add_action('admin_init', array($this, 'setup_sections'));
        add_action('admin_init', array($this, 'setup_fields'));
        add_action( 'woocommerce_checkout_process', 'wc_minimum_order_amount' );
        add_action( 'woocommerce_before_cart' , 'wc_minimum_order_amount' );
    }

    public function set_wc_minimum_order_amount() {
        // Set this variable to specify a minimum order value
    
        if ( WC()->cart->total < $this->$minimum_value ) {
    
            if( is_cart() ) {
    
                wc_print_notice( 
                    sprintf( 'Your current order total is %s — you must have an order with a minimum of %s to place your order ' , 
                        wc_price( WC()->cart->total ), 
                        wc_price( $this->$minimum_value )
                    ), 'error' 
                );
    
            } else {
    
                wc_add_notice( 
                    sprintf( 'Your current order total is %s — you must have an order with a minimum of %s to place your order' , 
                        wc_price( WC()->cart->total ), 
                        wc_price( $this->$minimum_value )
                    ), 'error' 
                );
            }
        }
    }

    public function minimum_order_page_content() { ?>
        <div class="wrap">
            <h2>Minimum Order Settings Page</h2>
            <form action="options.php" method="post">
                <?php
                    settings_fields('smashing_fields');
                    do_settings_sections('smashing_fields');
                    submit_button();
                ?>
            </form>
        </div>
    <?php }

    public function create_settings_page() {
        // Add the menu item and page
        $page_title = 'Minimum Order Settings';
        $menu_title = 'Minimum Order';
        $capability = 'manage_options';
        $slug = 'smashing_fields';
        $callback = array($this, 'minimum_order_page_content');
        $icon = 'dashicons-admin-plugins';
        $position = 100;

        add_submenu_page('options-general.php', $page_title, $menu_title, $capability, $slug, $callback, $icon, $position);
    }

    public function section_callback($arguments) {
        switch($arguments['id']) {
            case 'our_first_section':
                echo 'Simple General Settings';
                break;
        }
    }

    public function setup_sections() {
        add_settings_section('our_first_section', 'General Settings', array($this, 'section_callback'), 'smashing_fields');
    }

    public function field_callback( $arguments ) {
        $value = get_option($arguments['uid']); // Get the current value, if there is one

        if(!$value) { // If no value exists
            $value = $arguments['default']; // Set to our default
        }

        // Check which type of field we want
        switch($arguments['type']) {
            case 'text': // If it is a text field
                printf('<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', 
                    $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value);
                break;
            case 'textarea':
                printf('<textarea name="%1$s" id="%1$s" placeholder="%2$s" rows="5" cols="50">%3$s</textarea>',
                    $arguments['uid'], $arguments['placeholer'], $value
                );
                break;
            case 'select':
                if(!empty($arguments['options']) && is_array($arguments['options'])) {
                    $options_markup;
                    foreach($arguments['options'] as $key => $label) {
                        $options_markup .= sprintf('<option value="%s" %s>%s</option>', $key, selected($value, $key, false), $label);
                    }

                    printf('<select name="%1$s" id="%1$s">%2$s</select>', $arguments['uid'], $options_markup);
                }
                break;
        }

        // If there is help text
        if($helper = $arguments['helper']) {
            printf('<span class="helper"> %s</span>', $helper);
        }

        // If there is supplemental text
        if($supplemental = $arguments['supplemental']) {
            printf('<p class="description">%s</p>', $supplemental);
        }
    }

    public function setup_fields() {
        $fields = array(
            array(
                'uid' => 'our_first_field',
                'label' => 'Minimum Order Amount',
                'section' => 'our_first_section',
                'type' => 'text',
                'options' => false,
                'placeholder' => '0',
                'supplemental' => 'This is the minimum order amount you want for your WooCommerce Cart',
                'default' => '0'
            )
        );

        foreach( $fields as $field ){
            add_settings_field( $field['uid'], $field['label'], array( $this, 'field_callback' ), 'smashing_fields', $field['section'], $field );
            register_setting( 'smashing_fields', $field['uid'] );
        }
    }
}

new MinimumOrder();